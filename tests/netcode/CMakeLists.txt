add_executable(test_client test_client_udp.c )
target_link_libraries(test_client client_udp cmocka)

add_test(NAME test_client COMMAND test_client)

target_include_directories(test_client PUBLIC 
    ${CMAKE_SOURCE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMOCKA_PUBLIC_INCLUDE_DIRS}
)

set_target_properties(test_client
    PROPERTIES
    LINK_FLAGS "-Wl,--wrap=seek_matching_host_port,--wrap=write,--wrap=read"
)

add_executable(test_server test_server_udp.c)
target_link_libraries(test_server server_udp cmocka)
add_test(NAME test_server COMMAND test_server)
target_include_directories(test_server PUBLIC
    ${CMAKE_SOURCE_DIR}/netcode
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMOCKA_PUBLIC_INCLUDE_DIRS}
)
set_target_properties(test_server
    PROPERTIES
    LINK_FLAGS "-Wl,--wrap=seek_socket,--wrap=recvfrom,--wrap=init_getnameinfo,--wrap=send_back,--wrap=shutdown_response,--wrap=clear_response,--wrap=change_response_message"
)

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netcode/client_udp.h>

int __wrap_seek_matching_host_port(char *argv[])
{
    int socket_id = mock_type(int);
    printf("mock::seek_matching_host_port called: %d\n", socket_id);
    return socket_id;
}

int write(int where, char* what, size_t len);
int __wrap_write(int where, char* what, size_t len)
{
    int buff_wrote = mock_type(int);
    printf("mock::write called: %d \n", buff_wrote);
    if (buff_wrote != len)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

int read(int where, char* what, size_t len);
int __wrap_read(int where, char* what, size_t len)
{
    printf("mock::read called\n");
    int BUF_SIZE = mock_type(int);
    ssize_t nread = mock_type(int);
    if (nread <= BUF_SIZE && nread != -1)
        return EXIT_SUCCESS;

    return EXIT_FAILURE;
}

int mock_main(int argc, char *argv[])
{
    if (argc < 3)
        return EXIT_FAILURE;
    int mocked_socket_file_desriptor = seek_matching_host_port(argv);

    if (mocked_socket_file_desriptor == EXIT_FAILURE)
        return EXIT_FAILURE;

    int succes_write = write(mocked_socket_file_desriptor, "eyt", strlen("eyt"));
    if (succes_write == EXIT_FAILURE)
        return EXIT_FAILURE;

    int succes_read = read(mocked_socket_file_desriptor, "buffs", strlen("buffs"));

    if (succes_read != EXIT_SUCCESS)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

static void test_main_client_exit_f(void **state)
{
    char *argv[] = {"","",""};
    int rv = mock_main(2, argv);
    assert_int_equal(rv, EXIT_FAILURE);
}

static void test_main_client_exit_good(void **state)
{
    char *argv[] = {"","",""};
    will_return(__wrap_seek_matching_host_port, 0);
    will_return(__wrap_write, 3);
    will_return(__wrap_read, 5);
    will_return(__wrap_read, 5);
    int rv = mock_main(3, argv);
    assert_int_equal(rv, EXIT_SUCCESS);
}

static void test_main_client_socket_bad(void **state)
{
    char *argv[] = {"","",""};
    will_return(__wrap_seek_matching_host_port, EXIT_FAILURE);
    int rv = mock_main(3, argv);
    assert_int_equal(rv, EXIT_FAILURE);
}

static void test_main_client_read_bad(void **state)
{
    char *argv[] = {"","",""};
    will_return(__wrap_seek_matching_host_port, 0);
    will_return(__wrap_write, 3);
    will_return(__wrap_read, 5);
    will_return(__wrap_read, -1);
    int rv = mock_main(3, argv);
    assert_int_equal(rv, EXIT_FAILURE);
}

int main()
{
    const struct CMUnitTest tests[] = 
    {
        cmocka_unit_test(test_main_client_exit_f),
        cmocka_unit_test(test_main_client_exit_good),
        cmocka_unit_test(test_main_client_socket_bad),
        cmocka_unit_test(test_main_client_read_bad),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}

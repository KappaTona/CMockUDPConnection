#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <server_udp.h>


extern NameInfoObj init_getnameinfo(SocketData sc);
extern SocketData seek_socket(char *argv_1);
extern Message change_response_message(Response, char*,ssize_t);
extern bool send_back(Response const *r_msg);
extern bool shutdown_response(Response *respone);
extern void clear_response(Response *respone);
int mock_main(int argc, char *argv[]);

SocketData __wrap_seek_socket(char *argv_1)
{
    printf("mock::seek_socket called\n");
    function_called();
    return (SocketData){.is_valid=true};
}

int __wrap_recvfrom(int socket_file_descriptor,
        char *data_buffer, size_t buf_len, int zero, struct sockaddr *peer, socklen_t *perr_len)
{
    printf("mock::recvfrom called\n");
    return mock_type(int);
}

Message __wrap_change_response_message(Response* r, char* data_b, ssize_t nread)
{
    
    printf("mock::change_response_message called \n");
    function_called();
    return (Message){0};
}

bool __wrap_send_back(Response const *r_msg)
{
    printf("mock::send_back called\n");
    function_called();
    return true;
}
bool __wrap_shutdown_response(Response *respone)
{
    function_called();
    printf("mock::shutdown_response called\n");
    return true;
}

void __wrap_clear_response(Response *respone)
{
    printf("mock::clear_response called \n");
    function_called();
}

static void test_main_server_bad_arg(void **state)
{
    char *argv[] = {"","",""};
    int rv = mock_main(3, argv);
    assert_int_equal(rv, EXIT_FAILURE);
}

static void test_main_server_bad_recvf(void **state)
{
    char *argv[] = {"","",""};

    expect_function_call(__wrap_seek_socket);
    int expected_bad_recvfrom = -1;
    will_return(__wrap_recvfrom, expected_bad_recvfrom);


    int rv = mock_main(2, argv);
    assert_int_equal(rv, expected_bad_recvfrom);
}

NameInfoObj __wrap_init_getnameinfo(SocketData param)
{
    printf("mock::init_getnameinfo called\n");
    function_called();
    return (NameInfoObj){.is_valid=true};
}

static void test_main_server_good(void **state)
{
    char *argv[] = {"","",""};

    expect_function_call(__wrap_seek_socket);
    will_return(__wrap_recvfrom, 0);

    expect_function_call(__wrap_init_getnameinfo);
    expect_function_call(__wrap_change_response_message);
    expect_function_call(__wrap_send_back);
    expect_function_call(__wrap_shutdown_response);
    expect_function_call(__wrap_clear_response);

    int rv = mock_main(2, argv);
    assert_int_equal(rv, EXIT_SUCCESS);
}

int mock_main(int argc, char *argv[])
{
    printf("mock::main called\n");
    if (argc != 2)
        return EXIT_FAILURE;

    SocketData sd = seek_socket("hmm");

    int nread = recvfrom(12, "yep", strlen("yep"), 0, NULL, NULL);

    if (nread != -1)
    {
        NameInfoObj nio = init_getnameinfo((SocketData){.is_valid=true});
        Response r = {0};
        change_response_message((Response){0}, "", nread);
        send_back(&r);
        bool yes = shutdown_response(&r);
        if (yes)
            clear_response(&r);
    }
    else
    {
        return nread;
    }

    return EXIT_SUCCESS;
}



int main()
{
    const struct CMUnitTest tests[] = 
    {
        cmocka_unit_test(test_main_server_bad_arg),
        cmocka_unit_test(test_main_server_bad_recvf),
        cmocka_unit_test(test_main_server_good),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}

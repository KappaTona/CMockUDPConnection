#ifndef udp_server_h
#define udp_server_h
#include "config_defines.h"
#include <netdb.h>
#include <stdbool.h>


/* checked addrinfo struct 
 * getaddrinfo() returns a list of adress structures.
 * Try each address untill we successfully `man 2 bind`.
 * if `man 2 socket` or (`man 2 bind`) fails, we (close the socket
 * and) try the next address.
 *
 * @list is a pointer to `struct addrinfo`. Details: `man getaddrinfo`
 * @is_ok is a `bool` field should be set true if @list has
 * any not `NULL` element.
 *
 * Example:
 * in server_udp.c @make_addrinfo_obj
 */
typedef struct AddrinfoObj
{
    struct addrinfo *list;
    bool is_ok;
} AddrinfoObj;

/* Message struct contains `char *` @data and `
 * `ssizet` @nread. Used mainly for wrapping
 * VLA of characters resulted by `strndup(buf, len);`. 
 * `struct Message` used by implementation:
 * @init_message(char* buf, ssizet len);
 *  - calls strndup(buf, len) -> result saved into @data and @nread respectively.
 * @clear_message(Message*);
 * - calls `free` for @data
 * @change_response_message(struct Respinse*, char *buf, ssize_t nread);
 */
typedef struct Message
{
    char *data;
    ssize_t nread;
} Message;

/* If `NameInfoObj` valid then it contains information about
* - @is_valid is set to true and @error_code is not set
* - @host, @service and the peer's socket address `man socketadd`
* Else NameInfoObj contains errors
* - @is_valid is set to false, @error_code is set to the
* `man 3 getnameinfo` return value.
* 
* details in `man 3 getaddrinfo`
* `NI_MAXHOST`, `NI_MAXSERV` defined in <net-db.h>
*/
typedef struct NameInfoObj
{
    Message host; // size: NI_MAXHOST
    Message service; // size: NI_MAXSERV
    bool is_valid;
    int error_code; // contains `man 3 getnameinfo`  return value
} NameInfoObj;

/* SocketData holds the socket file descriptor and peer address and its length returned by `man getaddrinfo`. Details: @seek_socket(port);
 *
 * Members:
 * @socket_file_descriptor. Its value -1 if no uscces. `man getaddrinfo` EXAMPLE section `sfd` variable.
 * @is_valid is set to true if @seek_socket found a valid socket set to false otherwise.
 * @error_code is set to the return value of `man getaddrinfo`. Details: `gai_strerror(addrinfo_code)` 
 * @peer_addr and @peer_addr_len
 * These members used for `man getnameinfo`
 * `const struct sockaddr *restrict addr, socklen_t addrlen` parameters.
 *
 * EXAMPLE usage in server_udp.c :
 * @seek_socket, @recvfrom, @init_getnameinfo.
 */
typedef struct SocketData
{
    int socket_file_descriptor;
    bool is_valid;
    int error_code;
    struct sockaddr peer_addr;
    socklen_t peer_addr_len;
} SocketData;


/* Structure that holds module together.
 * Its members are somekind of wrappers of the
 * server program example found in `man getaddrinfo`. Used by @send_back(Response const*); found
 * in server_udp.c . Briefly if client succefully connected, and sent bytes it will print size, and message of the data. If the client sends the string "exit" @shutdown_response(Response*); will be called, and stops the infinite loop.
 *
 * Members:
 * - Message msg; Holds the data which will be send back
 * - SpcketData via; Via the opened socket file descriptor
 * - NameInfoObj *actor; to the host on port.
 */
typedef struct Response
{
    Message msg;
    SocketData via;
    NameInfoObj *actor;
} Response;

/* main_server function as the name suggests the main loop of the EXAMPLE server implementation
 * found in `man getaddrinfo`
 * `argv[1]` commandline paramter must be the port number.
 *
 * Usage:
 * ./server port
 */
extern int main_server(int argc, char *argv[]);

#endif //udp_server_h

#include "client_udp.h"
#include "config_defines.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>


int main_client(int argc, char *argv[])
{
    if (argc < 3)
    {
        fprintf(stderr, "Usage: %s host port msg...\n", argv[0]);
        return EXIT_FAILURE;
    }


    /* Send remaining command-line arguments as separate
     * datagrams, and read response from server.
     */

    int socket_file_desriptor = seek_matching_host_port(argv);
    if (socket_file_desriptor == EXIT_FAILURE)
        return EXIT_FAILURE;

    char buf[BUF_SIZE] = {0};
    for (int j = 3; j < argc; j++)
    {
        int len = strlen(argv[j]) + 1;
        if (write(socket_file_desriptor, argv[j], len) != len)
        {
            fprintf(stderr, "partial/failed write \n");
            return EXIT_FAILURE;
        }

        ssize_t nread = read(socket_file_desriptor, buf, BUF_SIZE);
        if (len <= BUF_SIZE && nread != -1)
        {
            printf("Received %zd bytes: %s\n", nread, buf);
            if (strncmp(buf, "exit", 4) == 0)
            {
                printf("Client willingly disconnected\n");
                return EXIT_SUCCESS;
            }
        }
        else
        {
            fprintf(stderr, "%s%d",
                    (nread == -1) ? "Socket file descriptor read error during argument: "
                    : " Ignoring long message in argument: \n",
                    j);
        }
    }

    return EXIT_SUCCESS;
}

int seek_matching_host_port(char *argv[])
{
    // Obtain address(es) matching host/port
    struct addrinfo hints = 
    {
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_DGRAM,
        .ai_flags = 0,
        .ai_protocol = 0,
    };
    struct addrinfo *address_structure_list = NULL;
    int addrinfo_code = getaddrinfo(argv[1], argv[2], &hints, &address_structure_list);
    if (addrinfo_code != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(addrinfo_code));
        return EXIT_FAILURE;
    }

    /* getaddrinfo() returns a list of address structures.
     * Try each address until we successfully `man 2 connect`.
     * If `man 2 socket` (or `man 2 connect`) fails, we (close the socket
     * and) try the next address.
     */

    int socket_file_desriptor = 0;
    struct addrinfo *item_of_addrinfo = address_structure_list;
    for (;item_of_addrinfo != NULL;
            item_of_addrinfo = address_structure_list->ai_next)
    {
        socket_file_desriptor = socket(item_of_addrinfo->ai_family,
                item_of_addrinfo->ai_socktype, item_of_addrinfo->ai_protocol);

        if (socket_file_desriptor == -1)
            continue;

        if (connect(socket_file_desriptor, item_of_addrinfo->ai_addr,
                        item_of_addrinfo->ai_addrlen) != -1)
        {
            break;
        }
        close(socket_file_desriptor);
    }

    freeaddrinfo(address_structure_list);

    if (item_of_addrinfo == NULL)
    {
        fprintf(stderr, "No address succeeded, Therefore could not connect\n");
        return EXIT_FAILURE;
    }

    return socket_file_desriptor;
}

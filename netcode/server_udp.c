#include "server_udp.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


static SocketData seek_socket(char *argv_1);
static NameInfoObj init_getnameinfo(SocketData *peer);
static Message change_response_message(Response *r, char *buf, ssize_t nread);
static bool send_back(Response const *r_msg);
static bool shutdown_response(Response *respone);
static void clear_response(Response *respone);


int main_server(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s port \n", argv[0]);
        return EXIT_FAILURE;
    }

    SocketData seeketh = seek_socket(argv[1]);

    // READ datagrams and echo them back to sendser.
    ssize_t nread = 0;
    char data_buffer[BUF_SIZE] = {0};

    bool abort_loop = false;
    while (!abort_loop)
    {
        nread = recvfrom(seeketh.socket_file_descriptor, data_buffer, BUF_SIZE, 0,
                &seeketh.peer_addr, &seeketh.peer_addr_len);

        if (nread != -1)
        {
            NameInfoObj nio = init_getnameinfo(&seeketh);
            printf("host: %s\n", nio.host.data);
            Response respone = { .actor=&nio, .via=seeketh, };
            respone.msg = change_response_message(&respone, data_buffer, nread);

            abort_loop = send_back(&respone) || shutdown_response(&respone);

            if (abort_loop)
                clear_response(&respone);
        }
        else
        {
            fprintf(stderr, "recvfrom failed\n");
            abort_loop = true;
        }
    }

    return EXIT_SUCCESS;
}

static Message init_message(char *buf, ssize_t nread)
{
    char *yep = strndup(buf, nread);
    return (Message){.data=yep, .nread=nread};
}

static socklen_t peer_addr_len()
{
    return sizeof(struct sockaddr_storage);
}

static NameInfoObj init_getnameinfo(SocketData *peer)
{
    char host[NI_MAXHOST] = {0}, service[NI_MAXSERV] = {0};
    int bytes_received_code = getnameinfo(&peer->peer_addr, peer->peer_addr_len,
        host, NI_MAXHOST, service, NI_MAXSERV, NI_NUMERICSERV);

    Message host_msg = init_message(host, strlen(host));
    Message service_msg = init_message(service, strlen(host));
    return bytes_received_code == 0 ?
        (NameInfoObj){.host=host_msg, .service=service_msg, .is_valid = true}
        : (NameInfoObj){.is_valid=false, .error_code=bytes_received_code};
}

static void clear_socket_data(SocketData *peer)
{
    if (peer->socket_file_descriptor != -1)
        close(peer->socket_file_descriptor);

    peer->socket_file_descriptor = -1;
}

static void clear_message(Message *msg)
{
    if (msg)
    {
        if (msg->data)
        {
            free(msg->data);
            msg->data = NULL;
        }
    }
}

static void clear_name_info_obj(NameInfoObj *obj)
{
    if (obj)
    {
        clear_message(&obj->host);
        clear_message(&obj->service);
    }
}

static void clear_response(Response *respone)
{
    if (respone)
    {
        clear_message(&respone->msg);
        clear_name_info_obj(respone->actor);
        clear_socket_data(&respone->via);
    }
}


static Message change_response_message(Response *r, char *buf, ssize_t nread)
{
    clear_message(&r->msg);
    return init_message(buf, nread);
}

static bool send_back(Response const *r_msg)
{
    NameInfoObj *actor = r_msg->actor;
    if (!actor->is_valid)
    {
        fprintf(stderr, "getnameinfo: %s\n", gai_strerror(actor->error_code));
        return true;
    }
    
    // non-fatal error
    Message msg = r_msg->msg;
    SocketData via = r_msg->via;
    if (sendto(via.socket_file_descriptor, msg.data, msg.nread, 0,
        &via.peer_addr, via.peer_addr_len) != msg.nread)
    {
        fprintf(stderr, "Non-fatal Error while sending response\n");
        return false;
    }

    printf("Received %zd bytes from %s:%s\n"
            "message as string: %s\n",
            msg.nread, actor->host.data, actor->service.data,
            msg.data);

    return false;
}

static AddrinfoObj make_addrinfo_obj(char const port[])
{
    struct addrinfo hints = 
    {
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_DGRAM,
        .ai_flags = AI_PASSIVE,
        .ai_protocol = 0,
        .ai_canonname = NULL,
        .ai_addr = NULL,
        .ai_next = NULL,
    };
    struct addrinfo *addrinfo_rv = NULL;
    int addrinfo_code = getaddrinfo(NULL, port, &hints, &addrinfo_rv);
    if (addrinfo_code != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(addrinfo_code));
        return (AddrinfoObj){NULL,false};
    }
    return (AddrinfoObj){addrinfo_rv, true};
}

static SocketData seek_socket(char *port)
{
    AddrinfoObj addrinfo = make_addrinfo_obj(port);
    if (!addrinfo.is_ok)
    {
        fprintf(stderr, "Memory issue\n");
        return (SocketData){.is_valid=false, .error_code=EXIT_FAILURE};
    }

    SocketData rv = {.is_valid=true, .error_code=0};
    rv.peer_addr_len = peer_addr_len();
    for (; addrinfo.list != NULL; addrinfo.list = addrinfo.list->ai_next)
    {
        rv.socket_file_descriptor = socket(addrinfo.list->ai_family,
                addrinfo.list->ai_socktype, addrinfo.list->ai_protocol);

        if (rv.socket_file_descriptor == -1)
            continue ;

        if (bind(rv.socket_file_descriptor, addrinfo.list->ai_addr,
                addrinfo.list->ai_addrlen) == 0)
        {
            break;
        }
        clear_socket_data(&rv);
    }

    freeaddrinfo(addrinfo.list);

    if (addrinfo.list == NULL)
    {
        fprintf(stderr, "Could not find any adrress for binding.\n");
        rv.is_valid = false;
        rv.error_code = EXIT_FAILURE;
        clear_socket_data(&rv);
        return rv;
    }

    return rv;
}

static bool shutdown_response(Response *respone)
{
    if (strncmp(respone->msg.data, "exit", 4) == 0 )
    {
        printf("host: %s disconnected and requested server shutdown\n",
                respone->actor->host.data);
        return true;
    }

    return false;
}
